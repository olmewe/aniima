import UIKit
import Foundation

class Bubble {
	let view: UIView
	let moldura: UIImageView
	let imageBackground: UIImageView

	var info: AnimationInfo
	var selected: Bool = false

	var disappearing: Bool = false

	static let moldura = UIImage(named:"moldura")

	var frame: CGRect

	init(v: UIView, info: AnimationInfo) {
		frame = Bubble.getRect(info.pos)
		view = UIView(frame:frame)
		self.info = info

		v.addSubview(view)

		view.dragShadowInit()

		let newSizeProp:CGFloat = 0.94
		imageBackground = UIImageView(frame:CGRect(
			x:Bubble.size*(1-newSizeProp)/2,
			y:Bubble.size*(1-newSizeProp)/2,
			width:Bubble.size*newSizeProp,
			height:Bubble.size*newSizeProp
		))
		imageBackground.backgroundColor = UIColor.white
		imageBackground.layer.masksToBounds = true
		imageBackground.layer.cornerRadius = imageBackground.frame.width/2
		view.addSubview(imageBackground)

		moldura = UIImageView(image:Bubble.moldura)
		moldura.frame = CGRect(x:0,y:0,width:Bubble.size,height:Bubble.size)
		view.addSubview(moldura)

		updateView()

		if info.new {
			info.new = false
			view.isHidden = true
		}
	}

	func updateFrame() {
		frame = Bubble.getRect(info.pos)
	}

	func updatePos(_ animate:Bool) {
		UIView.animate(withDuration: 0.2,delay:0,options:.curveEaseOut,animations:{
			self.view.frame = self.frame
		},completion:nil)
	}

	func updateView() {
		guard let image = Comum.loadAnimationPreview(info) else {
			imageBackground.image = nil
			moldura.tintColor = Color.convertIntToColor(Comum.paletteColors[Utils.randomNumber(0...7)])
			return
		}
		imageBackground.image = image
		let pixelData = image.cgImage!.dataProvider!.data
		let data:UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
		var sinSum:CGFloat = 0
		var cosSum:CGFloat = 0
		var asd = false
		for i in 0..<Int(image.size.width*image.size.height) {
			let color = Color.convertColorToHSB(Color.convertRGBToColor((data[i*4+0],data[i*4+1],data[i*4+2])))
			let m = color.s*color.b
			if m > 0 {
				let angle = color.h*CGFloat(M_PI*2)
				sinSum += sin(angle)*m
				cosSum += cos(angle)*m
				asd = true
			}
		}
		if asd {
			var hue = atan2(sinSum,cosSum)/CGFloat(M_PI*2)
			while hue < 0 { hue += 1 }
			while hue >= 1 { hue -= 1 }
			moldura.tintColor = UIColor(hue:hue,saturation:0.85,brightness:1,alpha:1)
		} else {
			moldura.tintColor = Color.convertIntToColor(Comum.paletteColors[Utils.randomNumber(0...7)])
		}
	}

	func bubbleAnimation() {
		Bubble.animatePress(view)
	}

	func appear(_ delay:Double = 0) -> Bool {
		if disappearing || !view.isHidden { return false }
		let duration:Double = 0.2
		let scale:CGFloat = 0.01
		view.isHidden = false
		view.transform = CGAffineTransform(scaleX: scale,y: scale)
		UIView.animate(withDuration: duration,delay:delay,options:.curveEaseOut,animations:{
			self.view.transform = CGAffineTransform(scaleX: 1,y: 1)
		},completion:nil)
		return true
	}

	func disappear() {
		if disappearing || view.isHidden { return }
		disappearing = true
		let duration:Double = 0.2
		let scale:CGFloat = 0.01
		UIView.animate(withDuration: duration,delay:0,options:.curveEaseIn,animations:{
			self.view.transform = CGAffineTransform(scaleX: scale,y: scale)
		}) {finished in
			self.view.removeFromSuperview()
		}
	}

	func setDragging(_ drag:Bool) {
		view.dragShadow(drag,resize:true)
	}

	////////

	static let distanceProportion:CGFloat = 0.15
	static let buttonSizeProportion:CGFloat = 1/3

	static var bubbleCount:Int = 0
	static var size:CGFloat = 0
	static var distance:CGFloat = 0
	static var totalWidth:CGFloat = 0
	static var totalHeight:CGFloat = 0
	static var buttonSize:CGFloat = 0

	static var sizeHalf:CGFloat = 0
	static var sizeHalfSquared:CGFloat = 0
	static var buttonSizeHalf:CGFloat = 0

	static func setFraming(_ view:UIView) {
		bubbleCount = (view.frame.width > 500) ? 4 : 3
		let bubFloat = CGFloat(bubbleCount)
		size = 2*view.frame.width/(2*distanceProportion*bubFloat+3*distanceProportion+2*bubFloat+1)
		distance = size*distanceProportion
		totalWidth = size+distance
		totalHeight = (size+distance)*0.866
		buttonSize = size*buttonSizeProportion

		sizeHalf = size/2
		sizeHalfSquared = sizeHalf*sizeHalf
		buttonSizeHalf = buttonSize/2
	}

	static func getRect(_ pos:Int) -> CGRect {
		//let pos = pos+1
		if pos < 0 {
			return CGRect(x:0,y:0,width:0,height:0)
		}
		let xInt = pos%bubbleCount
		let yInt = pos/bubbleCount
		let x:CGFloat
		if (yInt & 1) == 0 {
			x = distance+totalWidth*CGFloat(xInt)
		} else {
			x = sizeHalf+distance*1.5+totalWidth*CGFloat(xInt)
		}
		let y = distance+totalHeight*CGFloat(yInt)
		return CGRect(
			x:x,
			y:y,
			width:size,
			height:size
		)
	}

	static func getPos(_ point:CGPoint) -> Int {
		let y = max(Int((point.y-distance/2)/totalHeight),0)
		let xFloat:CGFloat
		if (y & 1) == 0 {
			xFloat = max(point.x-distance/2,0)
		} else {
			xFloat = max(point.x-distance-sizeHalf,0)
		}
		let x = min(Int(xFloat/totalWidth),bubbleCount-1)
		let pos = x+y*bubbleCount
		return pos//-1
	}

	static func animatePress(_ view:UIView) {
		let start:Double = 0.05
		let end:Double = 0.15
		let scale:CGFloat = 0.9
		UIView.animate(withDuration: start,delay:0,options:.curveLinear,animations:{
			view.transform = CGAffineTransform(scaleX: scale,y: scale)
		}) {finished in
			UIView.animate(withDuration: end,delay:0,options:.curveEaseOut,animations:{
				view.transform = CGAffineTransform(scaleX: 1,y: 1)
			},completion:nil)
		}
	}
}
