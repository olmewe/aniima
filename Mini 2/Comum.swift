import Foundation
import UIKit
import ImageIO
import MobileCoreServices

class Comum {
	static let defaultSize:Int = 128
	static let maxBrushSize:Int = 24
	static let defaultFps:Int = 12

	static let paletteColors:[Int] = [
		0xff3030, // 0
		0xffaf3c, // 1
		0xffea00, // 2
		0x3cff4a, // 3
		0x3ce1ff, // 4
		0x3052ff, // 5
		0xc13cff, // 6
		0xff3ce6, // 7

		0x333333,
		0x777777,
		0xbbbbbb,
		0xffffff,
	]

	static var animationPos = [Int]()
	static var animationList = [Int:AnimationInfo]()
	static let key = "Animations"
	fileprivate static var loaded = false
	fileprivate static var boolReturn = false

	static func loadAnimationInfo() {
		if loaded { return }
		loaded = true
		let defaults = UserDefaults.standard
		if let data = defaults.object(forKey: key) as? Data,let newList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Int:AnimationInfo] {
			animationList = newList
			animationPos = animationList.map { $0.0 }
			animationPos.sort()
			for (pos,info) in animationList {
				info.pos = pos
			}
		}
	}

	static func saveAnimationInfo() {
		let defaults = UserDefaults.standard
		defaults.set(NSKeyedArchiver.archivedData(withRootObject: animationList),forKey:key)
	}

	static func addAnimationInfo(_ info:AnimationInfo) {
		if animationList.updateValue(info,forKey:info.pos) == nil {
			animationPos.append(info.pos)
			animationPos.sort()
		}
		saveAnimationInfo()
	}

	static func removeAnimationInfo(_ remove:Int) {
		if animationList.removeValue(forKey: remove) != nil {
			if let offset = animationPos.index(of: remove) {
				animationPos.remove(at: offset)
			}
			saveAnimationInfo()
		}
	}

	static func moveAnimationInfo(_ from:Int,to:Int) {
		if let info = animationList.removeValue(forKey: from) {
			var add = to
			if animationList.updateValue(info,forKey:add) != nil {
				add = getNextPos()
				animationList.updateValue(info,forKey:add)
			}
			if let index = animationPos.index(of: from) {
				animationPos[index] = add
			} else {
				animationPos.append(add)
			}
			animationPos.sort()
		}
		saveAnimationInfo()
	}

	static func getNextPos() -> Int {
		var index:Int = 0
		for i in animationPos {
			if index != i {
				return index
			}
			index += 1
		}
		return index
	}

	static func getAnimationDataPath(_ info:AnimationInfo) -> URL {
		let documentsDirectory = FileManager.default.urls(for:.documentDirectory,in:.userDomainMask)[0]
		return documentsDirectory.appendingPathComponent(info.id)
	}

	static func saveAnimation(_ animation:Animation) -> Bool {
		let newAnimation:Bool
		if animation.info.id == "" {
			newAnimation = true
			let currentDate = Date()
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "EEEE_MMMM_dd_yyyy_HH-mm-ss"
			animation.info.id = dateFormatter.string(from: currentDate)
		} else {
			newAnimation = false
		}
		animation.info.date = Date()
		let dataPath = getAnimationDataPath(animation.info)
		do {
			if !newAnimation {
				try FileManager.default.removeItem(at:dataPath)
			}
			try FileManager.default.createDirectory(at:dataPath,withIntermediateDirectories:false,attributes:nil)

			var count = 0
			for image in animation.frames {
				let imagePath = dataPath.appendingPathComponent("/\(animation.info.id)_\(count).png")
				let pngImageData = UIImagePNGRepresentation(image)
				FileManager.default.createFile(atPath:imagePath.path,contents:pngImageData,attributes:nil)
				count += 1
			}
			print("url= \(dataPath)")

			if newAnimation {
				animation.info.pos = getNextPos()
				addAnimationInfo(animation.info)
			} else {
				saveAnimationInfo()
			}

			return true
		} catch let error as NSError {
			print(error.localizedDescription)
		}
		return false
	}

	static func deleteAnimation(_ info:AnimationInfo) -> Bool {
		let dataPath = getAnimationDataPath(info)
		do {
			try FileManager.default.removeItem(at:dataPath)
			removeAnimationInfo(info.pos)
			return true
		} catch let error as NSError {
			print(error.localizedDescription)
		}
		return false
	}

	static func loadAnimation(_ info:AnimationInfo) -> Animation? {
		let animation = Animation()
		if loadAnimation(info,animation:animation) {
			return animation
		}
		return nil
	}

	static func loadAnimation(_ info:AnimationInfo,animation:Animation) -> Bool {
		animation.info = info
		let dataPath = getAnimationDataPath(info)

		do {
			let directoryContents = try FileManager.default.contentsOfDirectory(at:dataPath,includingPropertiesForKeys:nil,options:[])
			let pngFiles = directoryContents.filter { $0.pathExtension == "png" }
			print("ALL IMAGES IN THE FOLDER = \(pngFiles)")

			var animationDictionary: [Int : UIImage] = [:]

			animation.frames.removeAll()
			for url in pngFiles {
				if let img = UIImage(contentsOfFile:url.path) {
					print("IMAGE = \(img)")
					let stringUrl: String = url.absoluteString
					print("STRING URL = \(stringUrl)")

					let num = imageIndex(path: stringUrl)
					if num >= 0{
						animationDictionary[num] = img
					}
				}
			}

			let sortedKeys = Array(animationDictionary.keys).sorted(by: <)
			for value in sortedKeys{
				if let imgDic = animationDictionary[value]{
					animation.frames.append(Utils.copyImage(imgDic))
				}
			}

			return !animation.frames.isEmpty
		} catch let error as NSError {
			print(error.localizedDescription)

			let alert = UIAlertController(title: "Erro", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.destructive, handler: {
				UIAlertAction in

				boolReturn = true

			}))
			let vc = MainViewController()
			vc.present(alert, animated: true, completion: nil)

		}
		while !boolReturn{
			print("Esperando")
		}
		return false
	}


	static func imageIndex(path: String) -> Int{
		let allDir = path.characters.split(separator: "/").map(String.init)
		let imageName = allDir.last

		if let auxImageName = imageName{
			let allNamePNG = auxImageName.characters.split(separator: "_").map(String.init)

			let imageIndexPNG = allNamePNG.last

			if let auxImageIndexName = imageIndexPNG{
				let splitIndexPNG = auxImageIndexName.characters.split(separator: ".").map(String.init)
				if let auxSplitIndexPNG = splitIndexPNG.first{
					let numberIndex = Int(auxSplitIndexPNG)
					if let auxNumberIndex = numberIndex{
						return auxNumberIndex
					}
				}
			}
		}
		return -1
	}




	static func loadAnimationPreview(_ info:AnimationInfo) -> UIImage? {
		let dataPath = getAnimationDataPath(info)
		do {
			let directoryContents = try FileManager.default.contentsOfDirectory(at: dataPath,includingPropertiesForKeys:nil,options:[])
			let pngFiles = directoryContents.filter { $0.pathExtension == "png" }

			for url in pngFiles {
				if let img = UIImage(contentsOfFile:url.path) {
					return Utils.copyImage(img)
				}
			}

			return nil
		} catch let error as NSError {
			print(error.localizedDescription)
		}
		return nil
	}

	static func createGif(_ info:AnimationInfo) -> (Data,URL)? {
		if let animation = loadAnimation(info) {
			return createGif(animation)
		}
		return nil
	}

	static func createGif(_ animation:Animation) -> (Data,URL)? {
		let fileProps:NSDictionary = [kCGImagePropertyGIFLoopCount as String:0]
		let fileProperties:NSDictionary = [kCGImagePropertyGIFDictionary as String:fileProps]

		//Define o tempo de cada quadro/frame.
		let frameProps:NSDictionary = [kCGImagePropertyGIFDelayTime as String:animation.frameDuration]
		let frameProperties:NSDictionary = [kCGImagePropertyGIFDictionary as String:frameProps]

		let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory,in:.userDomainMask)[0]
		let fileURL = documentsDirectoryURL.appendingPathComponent("\(animation.info.name).gif")

		print("url= \(fileURL)");
		if let destination = CGImageDestinationCreateWithURL(fileURL as CFURL,kUTTypeGIF,animation.frames.count,nil) {
			CGImageDestinationSetProperties(destination,fileProperties)
			for image in animation.frames {
				CGImageDestinationAddImage(destination,image.cgImage!,frameProperties)
			}
			if (!CGImageDestinationFinalize(destination)) {
				print("failed to finalize image destination");
			}
			if let data = try? Data(contentsOf: fileURL) {
				return (data,fileURL)
			}
		}
		return nil
	}
}
