import Foundation
import UIKit

class CanvasViewController:UIViewController,UIGestureRecognizerDelegate {
	@IBOutlet weak var frameImageView:UIImageView!
	@IBOutlet weak var frameOnionImageView:UIImageView!
	@IBOutlet weak var frameScrollView:UIScrollView!
	@IBOutlet weak var frameStack:UIStackView!

	@IBOutlet weak var toolPencilButton:UIButton!
	@IBOutlet weak var toolFillButton:UIButton!
	@IBOutlet weak var toolEraserButton:UIButton!

	@IBOutlet weak var onionSkinButton:UIButton!
	@IBOutlet weak var playButton:UIButton!
	@IBOutlet weak var pauseButton:UIButton!

	@IBOutlet weak var colorPickerButton:UIButton!
	@IBOutlet weak var brushSizeButton:UIButton!
	@IBOutlet weak var optionsButton:UIButton!

    @IBOutlet weak var menuView:UIView!
    @IBOutlet weak var menuBlockerView:UIView!

	@IBOutlet weak var colorPickerView:UIView!
    @IBOutlet weak var colorPickerHsb:UIImageView!
    @IBOutlet weak var colorPickerSlider:UIImageView!
    @IBOutlet weak var colorPickerHsbPick:UIImageView!
	@IBOutlet weak var colorPickerSliderPick:UIImageView!
	@IBOutlet weak var colorPickerBalloon:UIView!
    @IBOutlet weak var colorPickerBalloonBg:UIImageView!
    @IBOutlet weak var colorPickerPaletteLeftView:UIStackView!
    @IBOutlet weak var colorPickerPaletteRightView:UIStackView!
    var colorPickerPalette = [UIView]()

    @IBOutlet weak var brushSizeView:UIView!
    @IBOutlet weak var brushSizeSlider:UISlider!
    @IBOutlet weak var brushSizePreview:UIImageView!
	@IBOutlet weak var brushSizeLabel:UILabel!

	@IBOutlet weak var optionsView:UIView!
	@IBOutlet weak var optionsFps:UITextField!
	@IBOutlet weak var optionsName:UITextField!

	@IBOutlet var gesture:UILongPressGestureRecognizer!

    var openMenu:OpenMenu = .none
    var menuTransitioning:Int = 0
    let menuTransition:Double = 0.2
	var menuConstraint:NSLayoutConstraint? = nil

	var starting = true
	var frameTransition:Double = 0
	let frameTransition1:Double = 0.2
	let frameTransition2:Double = 0.125

    var canDraw = false
    var tool:Tool = .pencil
	var onionSkin:Bool = false

	let toolBgImage = UIImage(named:"iconselect")
	let menuBgImage = UIImage(named:"iconselect2")

    var color = UIColor.white
	var colorHSB:(h:CGFloat,s:CGFloat,b:CGFloat) = (0,0,0)
	var colorRGB:(r:UInt8,g:UInt8,b:UInt8) = (0,0,0)

	var brushSize:Int = 2
	var brushPattern:ANImage!
	var brushPatternFill = [(x:Int,y:Int)]()
	var brushPatternOutline = [(x:Int,y:Int)]()
	var brushPatternMin:Int = 0
	var brushPatternMax:Int = 0

    var touchMove:((Void) -> Void)? = nil
	var touchEnd:((Void) -> Void)? = nil

	var animation:Animation!
	var frameIndex:Int = 0
	let frame = ANImage()
    var timer = Timer()
	var playingAnimation: Bool = false
	var alreadySaved = true

	var frameView = [FrameView]()

    var historyComparison = ANImage()
    var historyUndo = [HistoryItem]()
    var historyRedo = [HistoryItem]()

    enum Tool {
        case pencil
        case fill
        case eraser
    }

    enum OpenMenu {
        case none
        case colorPicker
        case brushSize
		case options
    }

    /*** view initialization ***/

	override func viewDidLoad() {
        super.viewDidLoad()

		frameTransition = frameTransition1

		frameImageView.superview?.backgroundColor = Color.gray

		for s in frameStack.subviews {
			s.removeFromSuperview()
		}

        Utils.pointSample(frameImageView)
        Utils.pointSample(frameOnionImageView)
        Utils.pointSample(brushSizePreview)

		gesture.delegate = self

		colorPickerPalette.removeAll()
		colorPickerPalette.append(contentsOf: colorPickerPaletteLeftView.subviews)
		colorPickerPalette.append(contentsOf: colorPickerPaletteRightView.subviews)
		colorPickerPalette.sort { $0.center.x < $1.center.x && $0.center.y < $1.center.y }
		for i in 0..<colorPickerPalette.count {
			let view = colorPickerPalette[i]
			view.backgroundColor = Color.darkGray
			let sub = UIView()
			sub.backgroundColor = Color.convertIntToColor(Comum.paletteColors[i])
			sub.translatesAutoresizingMaskIntoConstraints = false
			view.addSubview(sub)

			view.addConstraint(NSLayoutConstraint(item:sub,attribute:.leading,relatedBy:.equal,toItem:view,attribute:.leading,multiplier:1,constant:2))
			view.addConstraint(NSLayoutConstraint(item:sub,attribute:.trailing,relatedBy:.equal,toItem:view,attribute:.trailing,multiplier:1,constant:-2))
			view.addConstraint(NSLayoutConstraint(item:sub,attribute:.top,relatedBy:.equal,toItem:view,attribute:.top,multiplier:1,constant:2))
			view.addConstraint(NSLayoutConstraint(item:sub,attribute:.bottom,relatedBy:.equal,toItem:view,attribute:.bottom,multiplier:1,constant:-2))
		}
	}

    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated)

        menuView.isHidden = true
        setMenuHeight(nil)
        menuBlockerView.isHidden = true
        menuBlockerView.alpha = 0

        colorPickerView.isHidden = true
        colorPickerBalloon.transform = CGAffineTransform(scaleX: 0,y: 0)

        brushSizeView.isHidden = true
        brushSizeSlider.minimumValue = 1
        brushSizeSlider.maximumValue = Float(Comum.maxBrushSize)
		brushPattern = ANImage(width:Comum.maxBrushSize,height:Comum.maxBrushSize)

		optionsView.isHidden = true

		setPlayPauseButton(false)
		switchTool(nil,.pencil)
		setOnionSkin(onionSkin)
    }

	override func viewDidAppear(_ animated:Bool) {
        super.viewDidAppear(animated)
        updateSize(false)
        colorRGB = (0x33,0x33,0x33)
		updateColor(.rgb)
		if animation == nil {
			animation = Animation()
		}
		if animation.frames.isEmpty {
			_ = createFrameDiff(0)
			frameScrollView.layoutIfNeeded()
		} else {
			recreateFrameViews()
			setFrame(0,edit:true)
			updateFrameViews()
		}
		optionsFps.text = "\(animation.info.fps)"
		optionsName.text = animation.info.name
		frameImageView.alpha = 0
		frameOnionImageView.alpha = 0
		UIView.animate(withDuration: frameTransition,delay:0,options:.curveEaseOut,animations:{
			self.frameImageView.alpha = 1
			self.frameOnionImageView.alpha = 0.1875
		},completion:nil)
		historyUndo.removeAll()
		historyRedo.removeAll()
		starting = false
		frameTransition = frameTransition2
		alreadySaved = true
	}

	override func viewWillDisappear(_ animated:Bool) {
		timer.invalidate()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		let undoMax = 16
		if historyUndo.count > undoMax {
			historyUndo.removeSubrange(undoMax..<historyUndo.count)
		}
	}

    /*** button interactions ***/

    @IBAction func tapColorPicker(_ sender:AnyObject) {
        setMenu(.colorPicker)
    }

    @IBAction func tapBrushSize(_ sender:AnyObject) {
        setMenu(.brushSize)
    }

    @IBAction func tapPencilTool(_ sender:AnyObject) {
        setTool(.pencil)
    }

    @IBAction func tapFillTool(_ sender:AnyObject) {
        setTool(.fill)
    }

    @IBAction func tapEraserTool(_ sender:AnyObject) {
        setTool(.eraser)
    }

    @IBAction func tapUndo(_ sender:AnyObject) {
        undoAction()
    }

    @IBAction func tapRedo(_ sender:AnyObject) {
        redoAction()
	}

	@IBAction func tapOptions(_ sender:AnyObject) {
		setMenu(.options)
	}

	@IBAction func tapNewFrame(_ sender:AnyObject) {
		createFrame(frameIndex+1)
	}

	@IBAction func tapCopyFrame(_ sender:AnyObject) {
		copyFrame(frameIndex)
	}

	@IBAction func tapDeleteFrame(_ sender:AnyObject) {
		deleteFrame(frameIndex)
	}

	@IBAction func tapOnionSkin(_ sender:AnyObject) {
		setOnionSkin(!onionSkin)
	}

	@IBAction func tapPlay(_ sender:AnyObject) {
		play()
	}

	@IBAction func tapPause(_ sender:AnyObject) {
		pause()
	}

    @IBAction func tapSave(_ sender:AnyObject) {
		if Comum.saveAnimation(animation) {
			alreadySaved = true
		} else {
			let newAlert = UIAlertController(title: "Error", message: "The animation couldn't be saved. Try again maybe?", preferredStyle: UIAlertControllerStyle.alert)
			newAlert.addAction(UIKit.UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil))
			self.present(newAlert,animated:true,completion:nil)
		}
	}

	@IBAction func tapBack(_ sender:AnyObject) {
		if alreadySaved {
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			let vc = storyboard.instantiateViewController(withIdentifier: "Main") as UIViewController
			present(vc, animated: true, completion: nil)
		} else {
			let alert = UIAlertController(title: "Unsaved changes", message: "Would you like to save changes before closing?", preferredStyle:UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: {
				UIAlertAction in

				if Comum.saveAnimation(self.animation) {
					let storyboard = UIStoryboard(name: "Main", bundle: nil)
					let vc = storyboard.instantiateViewController(withIdentifier: "Main") as UIViewController
					self.present(vc, animated: true, completion: nil)
				} else {
					let newAlert = UIAlertController(title: "Error", message: "The animation couldn't be saved. Try again maybe?", preferredStyle: UIAlertControllerStyle.alert)
					newAlert.addAction(UIKit.UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil))
					self.present(newAlert,animated:true,completion:nil)
				}

			}))
			alert.addAction(UIAlertAction(title: "Don't save", style: UIAlertActionStyle.destructive, handler: {
				UIAlertAction in

				let storyboard = UIStoryboard(name: "Main", bundle: nil)
				let vc = storyboard.instantiateViewController(withIdentifier: "Main") as UIViewController
				self.present(vc, animated: true, completion: nil)

			}))
			alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
				UIAlertAction in

				return
			}))
			self.present(alert, animated: true, completion: nil)
		}
	}

	@IBAction func closeTextFields(_ sender:AnyObject) {
		optionsName.endEditing(true)
		optionsFps.endEditing(true)
	}

	@IBAction func changeFps(_ sender:AnyObject) {
		if let intTest = Int(optionsFps.text!) {
			setAnimationFps(Utils.clamp(intTest,1,60))
		} else {
			optionsFps.text = "\(animation.info.fps)"
		}
	}

	@IBAction func changeName(_ sender:AnyObject) {
		setAnimationName(optionsName.text!)
	}

    /*** screen touch interactions (drawing, color picking, etc) ***/

	func gestureRecognizer(_ gestureRecognizer:UIGestureRecognizer,shouldRecognizeSimultaneouslyWith otherGestureRecognizer:UIGestureRecognizer) -> Bool {
		return true
	}

    @IBAction func longGesture(_ sender:AnyObject) {
        if menuTransitioning > 0 { return }
        if touchEnd != nil || touchMove != nil {
            if gesture.state == .ended || gesture.state == .failed || gesture.state == .cancelled {
                if let end = touchEnd { end() }
                touchEnd = nil
                touchMove = nil
            } else {
                if let move = touchMove { move() }
            }
            return
		} else if gesture.state != .began { return }
		if optionsFps.isFirstResponder {
			if !touchInside(optionsFps) {
				optionsFps.endEditing(true)
			}
			return
		}
		if optionsName.isFirstResponder {
			if !touchInside(optionsName) {
				optionsName.endEditing(true)
			}
			return
		}
        if openMenu != .none {
            let menu = getMenu(openMenu)!
            if touchInside(menu) {
                switch openMenu {
                case .colorPicker:
					let move:((Void) -> Void)
					if touchInside(colorPickerHsb) {
						move = {
							let pos = self.touchPos(self.colorPickerHsb)
							self.colorHSB.s = Utils.clamp(pos.x/self.colorPickerHsb.frame.width,0,1)
							self.colorHSB.b = Utils.clamp(1-pos.y/self.colorPickerHsb.frame.height,0,1)
							self.updateColor(.hsb)
						}
					} else if touchInside(colorPickerSlider) {
						move = {
							self.colorHSB.h = Utils.clamp(1-self.touchPos(self.colorPickerSlider).y/self.colorPickerSlider.frame.height,0,1)
							self.updateColor(.hsb)
						}
					} else {
                        var palette:Int = -1
                        for i in 0..<colorPickerPalette.count {
                            if touchInside(colorPickerPalette[i]) {
                                palette = i
                                break
                            }
                        }
                        if palette < 0 { break }
                        move = {}
                        colorRGB = Color.convertIntToRGB(Comum.paletteColors[palette])
                        updateColor(.rgb)
					}
					let animDuration = 0.15
					touchMove = {
                        move()
                        self.colorPickerBalloon.center = self.touchPos(self.colorPickerBalloon.superview!)
					}
                    touchEnd = {
                        self.touchMove!()
						UIView.animate(withDuration: animDuration,delay:0,options:.curveEaseIn,animations:{
							self.colorPickerBalloon.transform = CGAffineTransform(scaleX: 0.0001,y: 0.0001)
						},completion:nil)
                    }
                    touchMove!()
					UIView.animate(withDuration: animDuration,delay:0,options:.curveEaseOut,animations:{
						self.colorPickerBalloon.transform = CGAffineTransform(scaleX: 1,y: 1)
					},completion:nil)
                default: break
                }
            } else if touchInside(menuBlockerView) {
                setMenu(.none)
            }
            return
        }
        if canDraw && touchInside(frameImageView) {
            saveFrameForComparison()
            if tool == .fill {
				let pixel = touchPixel()
				frame.startTrackingChanges()
				frameFill(pixel.x,pixel.y,color:colorRGB)
				frame.stopTrackingChanges()
                addFrameDifferences()
            } else {
				let toolColor:(r:UInt8,g:UInt8,b:UInt8)
                let roundBrush:Bool
                if tool == .pencil {
                    toolColor = colorRGB
                    roundBrush = true
                } else {
                    toolColor = (0xff,0xff,0xff)
                    roundBrush = false
                }
				let pixel = touchPixel()
				frame.startTrackingChanges()
				if framePaint(pixel.x,pixel.y,round:roundBrush,color:toolColor,fill:true) {
					updateFrame(false)
				}
				var prev = pixel
                touchMove = {
                    let pixel = self.touchPixel()
                    if prev == pixel { return }
					self.framePaint(prev.x,prev.y,pixel.x,pixel.y,round:roundBrush,color:toolColor)
					prev = pixel
                }
                touchEnd = {
					self.frame.stopTrackingChanges()
					self.addFrameDifferences()
					self.updateFrame(true)
				}
            }
			return
		}
		if touchInside(frameScrollView) {
			var index:Int = 0
			for view in frameView {
				let fr = view.superview!.convert(view.frame,to:self.view)
				if fr.maxX < 0 {
					index += 1
					continue
				}
				if fr.minX >= self.view.frame.width { return }
				if touchInside(view) {
					break
				}
				index += 1
			}
			if index >= frameView.count { return }
			let scroll = frameScrollView.contentOffset.x
			let pos = touchPos(self.view)
			touchMove = {
				if self.frameScrollView.contentOffset.x != scroll {
					self.touchMove = nil
					return
				}
				let newPos = self.touchPos(self.view)
				if abs(newPos.x-pos.x) > 20 {
					self.touchMove = nil
					return
				}
				if abs(newPos.y-pos.y) > 20 {
					self.updateOnionSkin(true)
					self.frameScrollView.isScrollEnabled = false
					self.frameScrollView.isScrollEnabled = true
					let frameDrag = self.frameView[index]
					frameDrag.tapGesture.isEnabled = false
					frameDrag.tapGesture.isEnabled = true
					frameDrag.dragShadow(true)
					self.frameStack.removeArrangedSubview(frameDrag)
					self.view.addSubview(frameDrag)
					var constraints = [
						NSLayoutConstraint(item:frameDrag,attribute:.centerX,relatedBy:.equal,toItem:self.view,attribute:.leading,multiplier:1,constant:0),
						NSLayoutConstraint(item:frameDrag,attribute:.centerY,relatedBy:.equal,toItem:self.view,attribute:.top,multiplier:1,constant:0),
						NSLayoutConstraint(item:frameDrag,attribute:.height,relatedBy:.equal,toItem:self.frameScrollView,attribute:.height,multiplier:1,constant:0)
					]
					self.view.addConstraints(constraints)
					let setConstraints = {
						let pos = self.touchPos(self.view)
						constraints[0].constant = pos.x
						constraints[1].constant = pos.y
						frameDrag.layoutIfNeeded()
					}
					setConstraints()
					self.view.setNeedsLayout()
					UIView.animate(withDuration: self.frameTransition,delay:0,options:.curveEaseOut,animations:{
						self.view.layoutIfNeeded()
					},completion:nil)
					var newIndex = index
					self.touchMove = {
						frameDrag.removeConstraints(constraints)
						setConstraints()
						let n = Utils.clamp(Int((self.touchPos(self.frameScrollView).x+5)/(self.frameScrollView.frame.height)+0.5),0,self.animation.frames.count-1)
						if newIndex != n {
							newIndex = n
						}
					}
					self.touchEnd = {
						if newIndex != index {
							self.moveFrame(index,to:newIndex)
						}
						let fr = self.view.convert(frameDrag.frame,to:self.frameScrollView)
						self.view.removeConstraints(constraints)
						frameDrag.removeFromSuperview()
						self.frameStack.insertArrangedSubview(frameDrag,at:newIndex)
						frameDrag.frame = fr
						frameDrag.dragShadow(false)
						self.frameScrollView.setNeedsLayout()
						UIView.animate(withDuration: self.frameTransition,delay:0,options:.curveEaseOut,animations:{
							self.frameScrollView.layoutIfNeeded()
						},completion:nil)
						self.updateOnionSkin()
					}
				}
			}
			touchEnd = nil
		}
    }

    fileprivate func touchInside(_ view:UIView) -> Bool {
        return view.frame.contains(touchPos(view.superview!))
    }

    fileprivate func touchPos(_ view:UIView) -> CGPoint {
        return gesture.location(in: view)
    }

    fileprivate func touchPixel() -> (x:Int,y:Int) {
        let pos = touchPos(frameImageView)
        let x = pos.x*CGFloat(Comum.defaultSize)/frameImageView.frame.width
        let y = pos.y*CGFloat(Comum.defaultSize)/frameImageView.frame.height
        return (Int(x),Int(y))
    }

    /*** ANImage manipulations (drawing and showing) ***/

	fileprivate func updateFrame(_ slow:Bool) {
        let image = frame.createImage(slow)
		frameImageView.image = image
		animation.frames[frameIndex] = image
		frameView[frameIndex].image = image
    }

	fileprivate func framePaint(_ ox:Int,_ oy:Int,_ ix:Int,_ iy:Int,round:Bool,color:(r:UInt8,g:UInt8,b:UInt8)) {
		let dx = ix-ox
		let dy = iy-oy
		if dx >= -1 && dx <= 1 && dy >= -1 && dy <= 1 {
			if framePaint(ix,iy,round:round,color:color,fill:false) { updateFrame(false) }
			return
		}
		let ax = abs(dx)
		let ay = abs(dy)
		var update = false
		if ax > ay {
			let s = (dx > 0) ? 1 : -1
			let m = Float(dy)/Float(ax)
			for c in 1...ax {
				let x = ox+c*s
				let y = oy+Int(floor(Float(c)*m))
				if framePaint(x,y,round:round,color:color,fill:false) { update = true }
			}
		} else {
			let s = (dy > 0) ? 1 : -1
			let m = Float(dx)/Float(ay)
			for c in 1...ay {
				let x = ox+Int(floor(Float(c)*m))
				let y = oy+c*s
				if framePaint(x,y,round:round,color:color,fill:false) { update = true }
			}
		}
		if update { updateFrame(false) }
    }

	fileprivate func framePaint(_ x:Int,_ y:Int,round:Bool,color:(r:UInt8,g:UInt8,b:UInt8),fill:Bool) -> Bool {
		if brushSize == 1 {
			return frame.setSafeCheck(x,y,color:color)
		}
		var update = false
		if !round || brushSize <= 3 {
			if fill || brushSize <= 2 {
				for nx in brushPatternMin..<brushPatternMax {
					for ny in brushPatternMin..<brushPatternMax {
						if frame.setSafeCheck(x+nx,y+ny,color:color) { update = true }
					}
				}
			} else {
				for nx in brushPatternMin..<brushPatternMax {
					if frame.setSafeCheck(x+nx,y+brushPatternMin,color:color) { update = true }
					if frame.setSafeCheck(x+nx,y+brushPatternMax-1,color:color) { update = true }
				}
				for ny in (brushPatternMin+1)..<(brushPatternMax-1) {
					if frame.setSafeCheck(x+brushPatternMin,y+ny,color:color) { update = true }
					if frame.setSafeCheck(x+brushPatternMax-1,y+ny,color:color) { update = true }
				}
			}
		} else {
			if fill {
				for n in brushPatternFill {
					if frame.setSafeCheck(x+n.x,y+n.y,color:color) { update = true }
				}
			}
			for n in brushPatternOutline {
				if frame.setSafeCheck(x+n.x,y+n.y,color:color) { update = true }
			}
		}
		return update
	}

	fileprivate func frameFill(_ x:Int,_ y:Int,color:(r:UInt8,g:UInt8,b:UInt8)) {
		if x < 0 || x >= frame.width || y < 0 || y >= frame.height { return }
		let pos = frame.getPosAt(x,y)
		let target = frame.get(pos)
		if target == color { return }
		var indices = [Int]()
		indices.append(pos)
		while let index = indices.first {
			indices.remove(at:0)
			if !frame.isEqual(index,color:target) { continue }
			let y = index/frame.width
			let min = y*frame.width
			let max = min+frame.width
			var minX = index
			var maxX = index+1
			while minX >= min {
				if !frame.isEqual(minX,color:target) { break }
				frame.set(minX,color:color)
				minX -= 1
			}
			while maxX < max {
				if !frame.isEqual(maxX,color:target) { break }
				frame.set(maxX,color:color)
				maxX += 1
			}
			if y < frame.height-1 {
				var add = true
				for p in (minX+frame.width+1)...(maxX+frame.width-1) {
					if frame.isEqual(p,color:target) {
						if add && !indices.contains(p) {
							add = false
							indices.append(p)
						}
					} else {
						add = true
					}
				}
			}
			if y > 0 {
				var add = true
				for p in (minX-frame.width+1)...(maxX-frame.width-1) {
					if frame.isEqual(p,color:target) {
						if add && !indices.contains(p) {
							add = false
							indices.append(p)
						}
					} else {
						add = true
					}
				}
			}
		}
		updateFrame(true)
    }

	/*** history (undo/redo) ***/

	fileprivate func saveFrameForComparison() {
		historyComparison = ANImage(copy:frame)
	}

	fileprivate func getFrameDifferences() -> HistoryItem? {
		if frame.min < 0 {
			return HistoryItem()
		}
		var drawing = [Int]()
		var offset:Int = frame.min
		for i in frame.min...frame.max {
			if frame.isEqual(i,image:historyComparison) {
				offset += 1
			} else {
				if offset > 0 {
					drawing.append(0x1000000 | offset)
					offset = 0
				}
				drawing.append(Color.convertRGBToInt(historyComparison.get(i)))
			}
		}
		return HistoryItem(drawing:drawing,frame:frameIndex)
	}

	fileprivate func addFrameDifferences() {
		addDifferences(getFrameDifferences())
	}

    fileprivate func addDifferences(_ diff:HistoryItem?) {
		if let d = diff {
			alreadySaved = false
            historyUndo.append(d)
        }
        historyRedo.removeAll()
        debugHistory()
    }

	fileprivate func applyDifferences(_ diff:HistoryItem) -> HistoryItem? {
		alreadySaved = false
        switch diff.type {
		case .none:
			return HistoryItem()
		case .frameCreate:
			frame.importImage(diff.createFrame)
			let d = addFrameDiff(diff.createAt)
			setFrame(diff.createAt,edit:true)
			return d
		case .frameDelete:
			let d = deleteFrameDiff(diff.deleteAt)
			if d != nil {
				setFrame(diff.deleteAt,edit:true)
			}
			return d
		case .frameReorder:
			return moveFrameDiff(diff.reorderFrom,to:diff.reorderTo,moveViews:true)
		case .fps:
			return setAnimationFpsDiff(diff.fps)
		case .name:
			return setAnimationNameDiff(diff.name)
        case .drawing:
			setFrame(diff.frameIndex,edit:true)
			let drawing = diff.drawing
			if drawing.isEmpty {
				return HistoryItem(drawing:drawing,frame:frameIndex)
			}
			saveFrameForComparison()
			frame.startTrackingChanges()
			var offset:Int = 0
			for i in drawing {
				if (i & 0x1000000) != 0 {
					offset += (i & 0xffffff)
				} else {
					frame.set(offset,color:Color.convertIntToRGB(i))
					offset += 1
				}
			}
			updateFrame(true)
			frame.stopTrackingChanges()
			return getFrameDifferences()
        }
    }

    fileprivate func undoAction() {
        if historyUndo.isEmpty { return }
        let diff = historyUndo.removeLast()
        if let newDiff = applyDifferences(diff) {
            historyRedo.append(newDiff)
        }
        debugHistory()
    }

    fileprivate func redoAction() {
        if historyRedo.isEmpty { return }
        let diff = historyRedo.removeLast()
        if let newDiff = applyDifferences(diff) {
            historyUndo.append(newDiff)
        }
        debugHistory()
    }

    fileprivate func debugHistory() {
        print("current history: \(historyUndo.count) available undos, \(historyRedo.count) available redos.")
    }

	/*** animation info ***/

	fileprivate func setAnimationFps(_ fps:Int) {
		addDifferences(setAnimationFpsDiff(fps))
	}

	fileprivate func setAnimationFpsDiff(_ fps:Int) -> HistoryItem? {
		if animation.info.fps != fps {
			let oldFps = animation.info.fps
			animation.info.fps = fps
			optionsFps.text = "\(fps)"
			return HistoryItem(changeFps:oldFps)
		}
		return nil
	}

	fileprivate func setAnimationName(_ name:String) {
		addDifferences(setAnimationNameDiff(name))
	}

	fileprivate func setAnimationNameDiff(_ name:String) -> HistoryItem? {
		let name = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		if animation.info.name != name {
			let oldName = animation.info.name
			animation.info.name = name
			optionsName.text = name
			return HistoryItem(changeName:oldName)
		}
		return nil
	}

	/*** frame load/unload and playback ***/

	fileprivate func updateOnionSkin(_ dragging:Bool = false) {
		frameOnionImageView.isHidden = playingAnimation || frameIndex == 0 || !onionSkin
		if frameIndex > 0 {
			frameOnionImageView.image = animation.frames[frameIndex-1]
			frameView[frameIndex-1].selected = ((onionSkin && !dragging && !playingAnimation) ? .onion : .false)
		}
	}

	func setFrame(_ index:Int,edit:Bool) {
		setFrame(index)
		if edit {
			enableFrameEditing()
		}
	}

	func setFrame(_ index:Int) {
		let i = Utils.clamp(index,0,animation.frames.count-1)
		if frameIndex >= 0 && frameIndex < animation.frames.count {
			frameView[frameIndex].selected = .false
			if frameIndex > 0 { frameView[frameIndex-1].selected = .false }
		}
		frameIndex = i
		frameView[frameIndex].selected = .true
		frameImageView.image = animation.frames[i]
		updateOnionSkin()
		canDraw = false
	}

	fileprivate func play() {
		if animation.frames.count == 1 || playingAnimation { return }
		setPlayPauseButton(true)
		timer = Timer.scheduledTimer(timeInterval: animation.frameDuration,target:self,selector:#selector(playTimer),userInfo:nil,repeats:true)
		RunLoop.main.add(timer,forMode:RunLoopMode.commonModes)
		playingAnimation = true
		updateOnionSkin()
	}

	@objc dynamic fileprivate func playTimer() {
		var count = frameIndex+1
		if count >= frameView.count {
			count = 0
		}
		setFrame(count,edit:false)
	}

	fileprivate func pause() {
		if !playingAnimation { return }
		setPlayPauseButton(false)
		timer.invalidate()
		enableFrameEditing()
		playingAnimation = false
		updateOnionSkin()
	}

	fileprivate func setPlayPauseButton(_ playing:Bool) {
		playButton.setBackgroundImage((playing ? toolBgImage : nil),for:UIControlState())
		pauseButton.setBackgroundImage((!playing ? toolBgImage : nil),for:UIControlState())
	}

	fileprivate func enableFrameEditing() {
		if !canDraw {
			canDraw = true
			frame.importImage(animation.frames[frameIndex])
			frameScrollView.scrollRectToVisible(frameView[frameIndex].frame,animated:true)
		}
	}

	fileprivate func recreateFrameViews() {
		for i in 0..<animation.frames.count {
			newFrameView(i).image = animation.frames[i]
		}
		view.layoutIfNeeded()
	}

	fileprivate func updateFrameViews() {
		for i in 0..<frameView.count {
			frameView[i].updateIndex(i)
		}
	}

	fileprivate func newFrameView(_ index:Int) -> FrameView {
		let frame = Bundle.main.loadNibNamed("Frame",owner:nil,options:nil)?[0] as! FrameView
		frame.translatesAutoresizingMaskIntoConstraints = false
		frame.canvas = self
		frame.selected = .false
		frame.dragShadowInit()
		frameStack.insertArrangedSubview(frame,at:index)
		frame.addConstraint(NSLayoutConstraint(item:frame,attribute:.width,relatedBy:.equal,toItem:frame,attribute:.height,multiplier:1,constant:1))
		frameView.insert(frame,at:index)
		let start = index == 0 || starting
		if start {
			frame.alpha = 0
		}
		if !starting {
			let oldFrame:UIView
			if index > 0 {
				oldFrame = frameView[index-1]
			} else {
				oldFrame = frameView[0]
			}
			frame.frame = oldFrame.frame
			for i in 0..<frame.subviews.count {
				frame.subviews[i].frame = oldFrame.subviews[i].frame
			}
		}
		UIView.animate(withDuration: frameTransition,delay:0,options:.curveEaseOut,animations:{
			if start {
				frame.alpha = 1
			}
			if !self.starting {
				self.frameScrollView.setNeedsLayout()
				self.frameScrollView.layoutIfNeeded()
			}
		},completion:nil)
		return frame
	}

	fileprivate func createFrame(_ index:Int) {
		addDifferences(createFrameDiff(index))
	}

	fileprivate func copyFrame(_ index:Int) {
		addDifferences(addFrameDiff(index+1))
	}

	fileprivate func deleteFrame(_ index:Int) {
		addDifferences(deleteFrameDiff(index))
	}

	fileprivate func moveFrame(_ from:Int,to:Int) {
		addDifferences(moveFrameDiff(from,to:to,moveViews:false))
	}

	fileprivate func createFrameDiff(_ index:Int) -> HistoryItem {
		if !frameView.isEmpty {
			frameView[frameIndex].selected = .false
			if frameIndex > 0 { frameView[frameIndex-1].selected = .false }
		}
		for i in 0..<frame.length {
			frame.set(i,color:(0xff,0xff,0xff))
		}
		return addFrameDiff(index)
	}

	fileprivate func addFrameDiff(_ index:Int) -> HistoryItem {
		if !frameView.isEmpty {
			frameView[frameIndex].selected = .false
			if frameIndex > 0 { frameView[frameIndex-1].selected = .false }
		}
		animation.frames.insert(frame.createImage(true),at:index)
		newFrameView(index).image = animation.frames[index]
		setFrame(index,edit:true)
		updateFrameViews()
		return HistoryItem(deleteFrameAt:index)
	}

	fileprivate func deleteFrameDiff(_ index:Int) -> HistoryItem? {
		if animation.frames.count <= 1 { return nil }
		let frame = Utils.copyImage(animation.frames[index])
		animation.frames.remove(at: index)
		frameView.remove(at: index).removeFromSuperview()
		UIView.animate(withDuration: frameTransition,delay:0,options:.curveEaseOut,animations:{
			self.frameScrollView.layoutIfNeeded()
		},completion:nil)
		setFrame(index,edit:true)
		updateFrameViews()
		return HistoryItem(createFrame:frame,at:index)
	}

	fileprivate func moveFrameDiff(_ from:Int,to:Int,moveViews:Bool) -> HistoryItem? {
		if frameIndex == from {
			frameIndex = to
		} else if from < to {
			if frameIndex > from && frameIndex <= to {
				frameIndex -= 1
			}
		} else if to < from {
			if frameIndex < from && frameIndex >= to {
				frameIndex += 1
			}
		}
		animation.frames.insert(animation.frames.remove(at: from),at:to)
		let frame = frameView[from]
		if moveViews {
			self.frameStack.removeArrangedSubview(frame)
			self.frameStack.insertArrangedSubview(frame,at:to)
			self.frameScrollView.setNeedsLayout()
			UIView.animate(withDuration: frameTransition,delay:0,options:.curveEaseOut,animations:{
				self.frameScrollView.layoutIfNeeded()
			},completion:nil)
		}
		frameView.insert(frameView.remove(at: from),at:to)
		updateFrameViews()
		return HistoryItem(moveFrameFrom:to,to:from)
	}

	/*** toolbar animations and whatnot ***/

    fileprivate func setTool(_ tool:Tool) {
        if self.tool == tool { return }
		switchTool(self.tool,tool)
		self.tool = tool
    }

	fileprivate func getTool(_ tool:Tool) -> UIButton {
		switch tool {
		case .pencil: return toolPencilButton
		case .fill: return toolFillButton
		case .eraser: return toolEraserButton
		}
	}

	fileprivate func switchTool(_ old:Tool?,_ new:Tool) {
		if let o = old {
			getTool(o).setBackgroundImage(nil,for:UIControlState())
		}
		getTool(new).setBackgroundImage(toolBgImage,for:UIControlState())
	}

	fileprivate func setOnionSkin(_ enable:Bool) {
		onionSkin = enable
		onionSkinButton.setBackgroundImage(enable ? toolBgImage : nil,for:UIControlState())
		updateOnionSkin()
	}

	fileprivate func getMenu(_ menu:OpenMenu) -> UIView? {
		switch menu {
		case .colorPicker: return colorPickerView
		case .brushSize: return brushSizeView
		case .options: return optionsView
		default: return nil
		}
	}

	fileprivate func getMenuButton(_ menu:OpenMenu) -> UIButton? {
		switch menu {
		case .colorPicker: return colorPickerButton
		case .brushSize: return brushSizeButton
		case .options: return optionsButton
		default: return nil
		}
	}

	fileprivate func setMenu(_ menu:OpenMenu) {
        if menuTransitioning > 0 { return }
        if menu == .none {
            if openMenu == .none { return }
            hideMenu(getMenu(openMenu)!)
			getMenuButton(openMenu)?.setBackgroundImage(nil,for:UIControlState())
            openMenu = .none
        } else {
            let new = getMenu(menu)!
            if openMenu == .none {
				showMenu(new)
				getMenuButton(menu)?.setBackgroundImage(menuBgImage,for:UIControlState())
                openMenu = menu
            } else if openMenu == menu {
				hideMenu(new)
				getMenuButton(openMenu)?.setBackgroundImage(nil,for:UIControlState())
                openMenu = .none
            } else {
                let old = getMenu(openMenu)!
				switchMenu(old,to:new)
				getMenuButton(openMenu)?.setBackgroundImage(nil,for:UIControlState())
				getMenuButton(menu)?.setBackgroundImage(menuBgImage,for:UIControlState())
                openMenu = menu
            }
        }
    }

    fileprivate func showMenu(_ menu:UIView) {
        menuTransitioning += 1
        menuView.isHidden = false
		menuView.isUserInteractionEnabled = false
        menu.isHidden = false
        menuBlockerView.alpha = 0
        menuBlockerView.isHidden = false
        UIView.animate(withDuration: menuTransition,delay:0,options:UIViewAnimationOptions(),animations:{
            self.setMenuHeight(menu)
			self.menuBlockerView.alpha = 1
			self.updateColor(.uiColor)
			self.view.layoutIfNeeded()
        }) {completed in
            self.menuTransitioning -= 1
            self.menuView.isHidden = false
			self.menuView.isUserInteractionEnabled = true
            menu.isHidden = false
            self.menuBlockerView.alpha = 1
            self.menuBlockerView.isHidden = false
        }
    }

    fileprivate func switchMenu(_ old:UIView,to new:UIView) {
        menuTransitioning += 1
        menuView.isHidden = false
		menuView.isUserInteractionEnabled = false
        old.isHidden = false
        old.alpha = 1
        new.isHidden = false
        new.alpha = 0
        menuBlockerView.alpha = 1
        menuBlockerView.isHidden = false
        UIView.animate(withDuration: menuTransition,delay:0,options:UIViewAnimationOptions(),animations:{
            old.alpha = 0
            new.alpha = 1
			self.setMenuHeight(new)
			self.view.layoutIfNeeded()
        }) {completed in
            self.menuTransitioning -= 1
            self.menuView.isHidden = false
			self.menuView.isUserInteractionEnabled = true
            old.isHidden = true
            old.alpha = 1
            new.isHidden = false
            new.alpha = 1
            self.menuBlockerView.alpha = 1
            self.menuBlockerView.isHidden = false
        }
    }

    fileprivate func hideMenu(_ menu:UIView) {
        menuTransitioning += 1
        menuView.isHidden = false
		menuView.isUserInteractionEnabled = false
        menu.isHidden = false
        self.menuBlockerView.alpha = 1
        self.menuBlockerView.isHidden = false
        UIView.animate(withDuration: menuTransition,delay:0,options:UIViewAnimationOptions(),animations:{
            self.setMenuHeight(nil)
			self.menuBlockerView.alpha = 0
			self.view.layoutIfNeeded()
        }) {completed in
			self.menuTransitioning -= 1
            self.menuView.isHidden = true
			self.menuView.isUserInteractionEnabled = false
            menu.isHidden = true
            self.menuBlockerView.alpha = 0
            self.menuBlockerView.isHidden = true
        }
    }

	fileprivate func setMenuHeight(_ view:UIView?) {
		if let c = menuConstraint {
			self.view.removeConstraint(c)
		}
		if let v = view {
			menuConstraint = NSLayoutConstraint(item:menuView,attribute:.bottom,relatedBy:.equal,toItem:v,attribute:.bottom,multiplier:1,constant:1)
		} else {
			menuConstraint = NSLayoutConstraint(item:menuView,attribute:.height,relatedBy:.equal,toItem:nil,attribute:.notAnAttribute,multiplier:1,constant:0)
		}
		self.view.addConstraint(menuConstraint!)
    }

    /*** tool updating ***/

    @IBAction func updateSize(_ sender:AnyObject) {
        updateSize(true)
    }

    fileprivate func updateSize(_ fromValue:Bool) {
        if fromValue {
            let new = Int(brushSizeSlider.value)
            if brushSize == new { return }
            brushSize = new
        } else {
            brushSizeSlider.setValue(Float(brushSize),animated:false)
        }
		brushSizeLabel.text = "\(brushSize) px"
		if (brushSize & 1) == 0 {
			let half = Comum.maxBrushSize/2
			let sqrRadius:Int
			if brushSize == 2 {
				sqrRadius = 4
			} else if brushSize == 4 {
				sqrRadius = 6
			} else {
				sqrRadius = brushSize*brushSize/4+4
			}
			for x in 0..<half {
				for y in 0..<half {
					let xx = half-x
					let yy = half-y
					let hasBrush = xx*xx+yy*yy <= sqrRadius
					let color:(r:UInt8,g:UInt8,b:UInt8) = hasBrush ? (0,0,0) : (0xff,0xff,0xff)
					_ = brushPattern.setSafeCheck(x,y,color:color)
					_ = brushPattern.setSafeCheck(Comum.maxBrushSize-x-1,y,color:color)
					_ = brushPattern.setSafeCheck(Comum.maxBrushSize-x-1,Comum.maxBrushSize-y-1,color:color)
					_ = brushPattern.setSafeCheck(x,Comum.maxBrushSize-y-1,color:color)
				}
			}
		} else {
			let half = Comum.maxBrushSize/2+1
			let center = Float(Comum.maxBrushSize+1)/2
			let sqrRadius:Float
			if brushSize == 1 {
				sqrRadius = 1
			} else if brushSize == 3 {
				sqrRadius = 5
			} else {
				sqrRadius = Float(brushSize*brushSize)/4+4
			}
			for x in 0..<Comum.maxBrushSize {
				_ = brushPattern.setSafeCheck(x,0,color:(0xff,0xff,0xff))
			}
			for y in 1..<Comum.maxBrushSize {
				_ = brushPattern.setSafeCheck(0,y,color:(0xff,0xff,0xff))
			}
			for x in 1..<half {
				for y in 1..<half {
					let xx = center-Float(x)
					let yy = center-Float(y)
					let hasBrush = xx*xx+yy*yy <= sqrRadius
					let color:(r:UInt8,g:UInt8,b:UInt8) = hasBrush ? (0,0,0) : (0xff,0xff,0xff)
					_ = brushPattern.setSafeCheck(x,y,color:color)
					_ = brushPattern.setSafeCheck(Comum.maxBrushSize-x,y,color:color)
					_ = brushPattern.setSafeCheck(Comum.maxBrushSize-x,Comum.maxBrushSize-y,color:color)
					_ = brushPattern.setSafeCheck(x,Comum.maxBrushSize-y,color:color)
				}
			}
		}
		brushSizePreview.image = brushPattern.createImage(false)
		brushPatternMin = -brushSize/2
		brushPatternMax = brushPatternMin+brushSize
		brushPatternOutline.removeAll()
		brushPatternFill.removeAll()
		if brushSize > 3 {
			let offset = Comum.maxBrushSize/2
			let a = brushPatternMin+offset
			let b = a+brushSize
			for x in a..<b {
				for y in a..<b {
					if brushPattern.isEqual(x,y,color:(0,0,0)) {
						if (!brushPattern.isEqualSafe(x-1,y  ,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x-1,y-1,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x  ,y-1,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x+1,y-1,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x+1,y  ,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x+1,y+1,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x  ,y+1,color:(0,0,0)) ||
							!brushPattern.isEqualSafe(x-1,y+1,color:(0,0,0))) {
							brushPatternOutline.append((x-offset,y-offset))
						} else {
							brushPatternFill.append((x-offset,y-offset))
						}
					}
				}
			}
		}
    }

    fileprivate enum UpdateColorFrom {
        case hsb
        case uiColor
        case rgb
    }

	fileprivate func updateColor(_ from:UpdateColorFrom) {
		if from != .uiColor {
			if from == .hsb {
				color = Color.convertHSBToColor(colorHSB)
			} else {
				color = Color.convertRGBToColor(colorRGB)
			}
		}
		if from != .hsb {
			colorHSB = Color.convertColorToHSB(color)
		}
		if from != .rgb {
			colorRGB = Color.convertColorToRGB(color)
		}
		colorPickerHsbPick.center = CGPoint(
			x:colorPickerHsb.frame.minX+(colorHSB.s)*colorPickerHsb.frame.width,
			y:colorPickerHsb.frame.minY+(1-colorHSB.b)*colorPickerHsb.frame.height
		)
		colorPickerSliderPick.center = CGPoint(
			x:colorPickerSliderPick.center.x,
			y:colorPickerSlider.frame.minY+(1-colorHSB.h)*colorPickerSlider.frame.height
		)
		colorPickerBalloonBg.tintColor = color
		colorPickerHsb.backgroundColor = Color.convertHSBToColor((colorHSB.h,1,1))
	}

    /*** helper class for history items ***/

	class HistoryItem {
		enum HistoryType {
            case none
			case drawing
			case frameReorder
			case frameCreate
			case frameDelete
			case fps
			case name
		}

		let type:HistoryType

		fileprivate let draw:[Int]?
		fileprivate let image:UIImage?
		fileprivate let int1:Int
		fileprivate let int2:Int
		fileprivate let string:String?

		//drawing
		var drawing:[Int] { get { return draw! } }
		var frameIndex:Int { get { return int1 } }

		//frame reorder
		var reorderFrom:Int { get { return int1 } }
		var reorderTo:Int { get { return int2 } }

		//frame create
		var createFrame:UIImage { get { return image! } }
		var createAt:Int { get { return int1 } }

		//frame delete
		var deleteAt:Int { get { return int1 } }

		//fps
		var fps:Int { get { return int1 } }

		//color
		var color:Int { get { return int1 } }

		//name
		var name:String { get { return string! } }

        init() {
			type = .none
			draw = nil
			image = nil
			int1 = 0
			int2 = 0
			string = nil
        }

		init(drawing:[Int],frame:Int) {
			type = .drawing
			draw = drawing
			image = nil
			int1 = frame
			int2 = 0
			string = nil
		}

		init(moveFrameFrom from:Int,to:Int) {
			type = .frameReorder
			draw = nil
			image = nil
			int1 = from
			int2 = to
			string = nil
		}

		init(createFrame frame:UIImage,at:Int) {
			type = .frameCreate
			draw = nil
			image = frame
			int1 = at
			int2 = 0
			string = nil
		}

		init(deleteFrameAt at:Int) {
			type = .frameDelete
			draw = nil
			image = nil
			int1 = at
			int2 = 0
			string = nil
		}

		init(changeFps fps:Int) {
			type = .fps
			draw = nil
			image = nil
			int1 = fps
			int2 = 0
			string = nil
		}

		init(changeName name:String) {
			type = .name
			draw = nil
			image = nil
			int1 = 0
			int2 = 0
			string = name
		}
	}
}
