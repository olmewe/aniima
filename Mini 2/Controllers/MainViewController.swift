import Foundation
import UIKit
import ImageIO
import MobileCoreServices

class MainViewController:UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var emptyAlert: UIView!

	@IBOutlet weak var previewFade: UIView!
	@IBOutlet weak var previewView: UIView!
	@IBOutlet weak var previewImageView: UIImageView!
	@IBOutlet weak var previewName: UILabel!
	@IBOutlet weak var previewDate: UILabel!
	@IBOutlet weak var previewActions: UIView!
	@IBOutlet var previewButtons: [UIButton]!

	var bubbles = [Bubble]()
	var bubbleSelected: Bubble?
	var bubbleSelectedIndex: Int?
	var bubbleSelectedAnimation: Animation?

	var dragging: Bubble?
	var dragIgnore = false
	var dragPos: Int = -1
	var dragOffset: CGPoint = CGPoint.zero
	var dragIndicator: UIImageView!

	var animating = false

	static var offset:CGFloat = 0

	override func viewDidLoad() {
		super.viewDidLoad()

		Comum.loadAnimationInfo()

		scrollView.contentSize.width = 1
		Bubble.setFraming(view)

		//arruma a view de alerta caso não tenha nenhuma animação na galeria
		emptyAlert.clipsToBounds = true
		emptyAlert.isHidden = true
		emptyAlert.alpha = 0

		//bolha pra indicar o lugar pra onde a bolha vai ao ser arrastada
		dragIndicator = UIImageView(image:UIImage(named:"molduraDrag"))
		dragIndicator.alpha = 0
		scrollView.addSubview(dragIndicator)

		//gesture para reconhecer em qual bolha estou clicando (serve para todas as bolhas menos a principal)
		let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
		tap.numberOfTouchesRequired = 1
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)

		//gesture para arrastar as bolhas
		let long = UILongPressGestureRecognizer(target: self, action: #selector(handleLong(_:)))
		long.numberOfTouchesRequired = 1
		long.minimumPressDuration = 0.25
		long.cancelsTouchesInView = false
		view.addGestureRecognizer(long)
		long.delegate = self

		//cria as bolhas e os botões
		for (_,info) in Comum.animationList {
			let bubble = Bubble(v: scrollView, info: info)
			bubbles.append(bubble)
		}
		updateContentHeight()
		scrollView.contentOffset = CGPoint(x:0,y:MainViewController.offset)

		//seta as views de preview
		previewFade.isHidden = true
		previewFade.alpha = 0
		previewView.isHidden = true
		previewView.alpha = 1
		previewImageView.layer.cornerRadius = previewImageView.frame.width/2
		previewImageView.layer.masksToBounds = true
		previewActions.isHidden = true
		for i in previewButtons {
			i.imageView!.contentMode = .scaleAspectFit
		}
		Utils.pointSample(previewImageView)
		animating = false
	}

	override func viewDidAppear(_ animated:Bool) {
		var delay:Double = 0
		for bub in bubbles {
			if bub.appear(delay) {
				delay += 0.04
			}
		}
		if bubbles.isEmpty {
			showEmptyAlert()
		}
	}

	override func viewWillDisappear(_ animated:Bool) {
		MainViewController.offset = scrollView.contentOffset.y
	}

	override func viewDidLayoutSubviews() {

	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		//Dispose of any resources that can be recreated.
	}

	func showEmptyAlert() {
		emptyAlert.isHidden = false
		emptyAlert.alpha = 0
		emptyAlert.layer.cornerRadius = emptyAlert.frame.height/4
		UIView.animate(withDuration: 0.5,delay:0,options:.curveEaseOut,animations: {
			self.emptyAlert.alpha = 1
		},completion:nil)
	}

	func updateContentHeight() {
		var max:CGFloat = 0
		let sd = Bubble.size+Bubble.distance*2
		for bub in bubbles where !bub.disappearing {
			let bubMax = bub.frame.maxY+sd
			if max < bubMax { max = bubMax }
		}
		let offset = scrollView.contentOffset.y
		scrollView.contentSize.height = max
		let limit = Swift.max(max-scrollView.frame.height,0)
		if offset > limit {
			scrollView.contentOffset.y = offset
			scrollView.setContentOffset(CGPoint(x:0,y:limit),animated:true)
		}
	}

	func gestureRecognizer(_ gestureRecognizer:UIGestureRecognizer,shouldRecognizeSimultaneouslyWith otherGestureRecognizer:UIGestureRecognizer) -> Bool {
		return true
	}

	//

	func handleTap(_ sender: UIPanGestureRecognizer) {
		guard !animating && bubbleSelected == nil else { return }
		let pos = sender.location(in: scrollView)
		if dragging != nil {
			stopDragging(pos,move:true)
			return
		}
		let result = getBubbleAt(pos)
		if let bub = result.bubble,let index = result.index {
			openBubble(bub,at:index)
		}
	}

	func handleLong(_ sender: UILongPressGestureRecognizer) {
		guard !animating && bubbleSelected == nil else { return }
		let pos = sender.location(in: scrollView)
		if !dragIgnore && dragging == nil {
			if sender.state == .began || sender.state == .changed {
				startDragging(pos)
			}
		} else {
			switch sender.state {
			case .began,.changed: updateDragging(pos)
			case .cancelled,.failed: stopDragging(pos,move:false)
			case .ended:
				stopDragging(pos,move:true)
			default: break
			}
		}
	}

	func startDragging(_ point:CGPoint) {
		let result = getBubbleAt(point)
		guard let bub = result.bubble,let i = result.index else {
			dragIgnore = true
			dragging = nil
			return
		}
		if let si = bubbleSelectedIndex {
			if si == i {
				dragIgnore = true
				dragging = nil
				return
			}
		}
		dragPos = -1
		dragging = bub
		scrollView.bringSubview(toFront: dragIndicator)
		scrollView.bringSubview(toFront: bub.view)
		scrollView.isScrollEnabled = false
		bub.setDragging(true)
		dragOffset = CGPoint(
			x:bub.view.frame.minX-point.x,
			y:bub.view.frame.minY-point.y
		)
		updateDragging(point)
		UIView.animate(withDuration: 0.1, animations: {
			self.dragIndicator.alpha = 0.1
		})
		return
	}

	func updateDragging(_ point:CGPoint) {
		guard let bub = dragging else { return }
		bub.view.frame = CGRect(
			x:point.x+dragOffset.x,
			y:point.y+dragOffset.y,
			width:Bubble.size,
			height:Bubble.size
		)
		let pos = Bubble.getPos(CGPoint(x:bub.view.frame.midX,y:bub.view.frame.midY))
		if pos >= 0 {
			if dragPos != pos {
				dragPos = pos
				dragIndicator.frame = Bubble.getRect(pos)
			}
		}
	}

	func stopDragging(_ point:CGPoint,move:Bool) {
		guard let bub = dragging else {
			dragIgnore = false
			return
		}
		dragging = nil
		var originalBubble:Bubble?
		if move {
			updateDragging(point)
			if bub.info.pos != dragPos {
				for i in bubbles where i.info.pos == dragPos {
					originalBubble = i
					break
				}
				if let org = originalBubble {
					let temp = Comum.getNextPos()
					let ret = bub.info.pos
					Comum.moveAnimationInfo(dragPos,to:temp)
					Comum.moveAnimationInfo(ret,to:dragPos)
					Comum.moveAnimationInfo(temp,to:ret)
					org.info.pos = ret
					org.updateFrame()
				} else {
					Comum.moveAnimationInfo(bub.info.pos,to:dragPos)
				}
				bub.info.pos = dragPos
				bub.updateFrame()
			}
			updateContentHeight()
		}
		bub.setDragging(false)
		bub.updatePos(true)
		if let org = originalBubble {
			org.updatePos(true)
			scrollView.bringSubview(toFront: org.view)
		}
		scrollView.bringSubview(toFront: dragIndicator)
		scrollView.bringSubview(toFront: bub.view)
		scrollView.isScrollEnabled = true
		dragIgnore = false
		UIView.animate(withDuration: 0.1, animations: {
			self.dragIndicator.alpha = 0
		})
	}

	func getBubbleAt(_ point:CGPoint) -> (bubble:Bubble?,index:Int?) {
		let pos = Bubble.getPos(point)

		var bubble:Bubble?
		var index:Int?

		for (i,bub) in bubbles.enumerated() where bub.info.pos == pos {
			let x = point.x-bub.view.frame.midX
			let y = point.y-bub.view.frame.midY
			if x*x+y*y <= Bubble.sizeHalfSquared {
				bubble = bub
				index = i
			}
			break
		}

		return (bubble,index)
	}

	//

	func openBubble(_ bubble:Bubble,at index:Int) {
		guard !animating && bubbleSelected == nil else { return }
		if let animation = Comum.loadAnimation(bubble.info) {

			bubbleSelected = bubble
			bubbleSelectedIndex = index
			bubbleSelectedAnimation = animation

			previewFade.isHidden = false
			previewView.isHidden = false
			animating = true

			previewImageView.image = bubble.imageBackground.image
			previewImageView.animationImages = animation.frames
			previewImageView.animationDuration = animation.frameDuration*Double(animation.frames.count)
			previewName.text = animation.info.name

			let dateFormatter = DateFormatter()
			dateFormatter.dateStyle = .medium
			dateFormatter.timeStyle = .short
			dateFormatter.locale = Locale.current

			previewDate.text = dateFormatter.string(from: animation.info.date)
			previewActions.isHidden = false

			let frame = previewActions.frame
			previewActions.frame = frame.offsetBy(dx:0,dy:frame.height)

			let center = previewView.center
			previewView.center = scrollView.convert(bubble.view.center,to:view)
			let scale = Bubble.size/view.frame.width
			previewView.transform = CGAffineTransform(scaleX: scale,y: scale)
			UIView.animate(withDuration: 0.25,delay:0,options:.curveEaseOut,animations:{
				self.previewFade.alpha = 1
				self.previewActions.frame = frame
				self.previewView.center = center
				self.previewView.transform = CGAffineTransform(scaleX: 1,y: 1)
			}) {completed in
				self.animating = false
				self.previewImageView.startAnimating()
			}
			previewImageView.layer.cornerRadius = 0
			let anim = CABasicAnimation.init(keyPath:"cornerRadius")
			anim.duration = 0.25
			anim.fromValue = previewImageView.frame.width/2
			anim.toValue = previewImageView.layer.cornerRadius
			anim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
			previewImageView.layer.add(anim,forKey:"cornerRadius")
		} else {
			let newAlert = UIAlertController(title: "Error", message: "The animation couldn't be loaded.", preferredStyle: UIAlertControllerStyle.alert)
			newAlert.addAction(UIKit.UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil))
			self.present(newAlert,animated:true,completion:nil)
		}
	}

	@IBAction func closeBubble() {
		closeBubble(false)
	}

	func closeBubble(_ fade:Bool) {
		guard !animating && bubbleSelected != nil else { return }
		animating = true
		let frame = previewActions.frame
		let center = previewView.center
		UIView.animate(withDuration: fade ? 0.1 : 0.25,delay:0,options:.curveEaseOut,animations:{
			self.previewFade.alpha = 0
			self.previewActions.frame = frame.offsetBy(dx:0,dy:frame.height)
			if fade {
				self.previewView.alpha = 0
			} else {
				self.previewView.center = self.scrollView.convert(self.bubbleSelected!.view.center,to:self.view)
				let scale = Bubble.size/self.view.frame.width
				self.previewView.transform = CGAffineTransform(scaleX: scale,y: scale)
			}
		}) {completed in
			self.previewFade.isHidden = true
			self.previewView.isHidden = true
			self.previewActions.isHidden = true
			self.animating = false
			self.previewActions.frame = frame
			if fade {
				self.previewView.alpha = 1
			} else {
				self.previewView.center = center
				self.previewView.transform = CGAffineTransform(scaleX: 1,y: 1)
			}
		}
		previewImageView.layer.cornerRadius = previewImageView.frame.width/2
		let anim = CABasicAnimation.init(keyPath:"cornerRadius")
		anim.duration = 0.25
		anim.fromValue = 0
		anim.toValue = previewImageView.layer.cornerRadius
		anim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		previewImageView.layer.add(anim,forKey:"cornerRadius")
		bubbleSelected = nil
		bubbleSelectedIndex = nil
	}

	//

	@IBAction func pressedAddBubble(_ sender : UIButton!) {
		let storyboard = UIStoryboard(name: "Canvas", bundle: nil)
		let vc = storyboard.instantiateViewController(withIdentifier: "Canvas") as! CanvasViewController
		vc.animation = nil
		present(vc, animated: true, completion: nil)
	}

	@IBAction func pressedBubbleEditar(_ sender : UIButton!) {
		guard let bub = bubbleSelectedAnimation else { return }

		let storyboard = UIStoryboard(name: "Canvas", bundle: nil)
		let vc = storyboard.instantiateViewController(withIdentifier: "Canvas") as! CanvasViewController
		vc.animation = bub
		present(vc, animated: true, completion: nil)
	}

	@IBAction func pressedBubbleCompartilhar(_ sender : UIButton!) {
		guard let bub = bubbleSelected else { return }

		if let shareData = Comum.createGif(bub.info) {
			let activity = UIActivityViewController(activityItems: [shareData.0], applicationActivities: nil)
			activity.completionWithItemsHandler = {(activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in

				if (!completed) {
					print("Cancelado")
					return
				}
				else{
					do {
						try FileManager.default.removeItem(at: shareData.1)
					} catch let error {
						print(error.localizedDescription)
					}
				}
			}
			if let popover = activity.popoverPresentationController {
				popover.sourceView = sender
				popover.sourceRect = CGRect(x:sender.frame.width/2,y:0,width:1,height:sender.frame.height)
			}
			present(activity, animated: true, completion: nil)
		} else {
			let newAlert = UIAlertController(title: "Error", message: "Something wrong happened when trying to share.", preferredStyle: UIAlertControllerStyle.alert)
			newAlert.addAction(UIKit.UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil))
			self.present(newAlert,animated:true,completion:nil)
		}

	}

	@IBAction func pressedBubbleApagar(_ sender : UIButton!) {
		guard let bub = bubbleSelected else { return }

		let alert = UIAlertController(title: "Delete animation", message: "Would you like to delete this animation?", preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: {
			UIAlertAction in

			if Comum.deleteAnimation(bub.info) {
				self.closeBubble(true)
				bub.disappear()
				self.updateContentHeight()
				if let index = self.bubbleSelectedIndex {
					self.bubbles.remove(at: index)
					if self.bubbles.isEmpty {
						self.showEmptyAlert()
					}
				}
			} else {
				let newAlert = UIAlertController(title: "Error", message: "Something wrong happened when trying to delete.", preferredStyle: UIAlertControllerStyle.alert)
				newAlert.addAction(UIKit.UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil))
				self.present(newAlert,animated:true,completion:nil)
			}

		}))
		alert.addAction(UIAlertAction(title: "Don't delete", style: UIAlertActionStyle.default, handler: {
			UIAlertAction in

			return
		}))
		self.present(alert, animated: true, completion: nil)

	}
}
