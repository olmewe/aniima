import Foundation
import UIKit

class Color {
	static let red = Color.convertIntToColor(0xff3030)
	static let yellow = Color.convertIntToColor(0xffea00)
	static let blue = Color.convertIntToColor(0x3052ff)
	static let darkGray = Color.convertIntToColor(0x333333)
	static let gray = Color.convertIntToColor(0x888888)
	static let lightGray = Color.convertIntToColor(0xbbbbbb)

	static let frameBorder = gray
	static let frameBorderUnselected = lightGray
	static let frameBorderSelected = blue
	static let frameBorderOnion = red

	static func convertHSBToColor(_ hsb:(h:CGFloat,s:CGFloat,b:CGFloat)) -> UIColor {
		return UIColor(hue:hsb.h,saturation:hsb.s,brightness:hsb.b,alpha:1)
	}

	static func convertColorToHSB(_ color:UIColor) -> (h:CGFloat,s:CGFloat,b:CGFloat) {
		var h:CGFloat = 0
		var s:CGFloat = 0
		var b:CGFloat = 0
		var a:CGFloat = 0
		color.getHue(&h,saturation:&s,brightness:&b,alpha:&a)
		return (h,s,b)
	}

	static func convertRGBToColor(_ rgb:(r:UInt8,g:UInt8,b:UInt8)) -> UIColor {
		return UIColor(red:CGFloat(rgb.r)/255,green:CGFloat(rgb.g)/255,blue:CGFloat(rgb.b)/255,alpha:1)
	}

	static func convertColorToRGB(_ color:UIColor) -> (r:UInt8,g:UInt8,b:UInt8) {
		var r:CGFloat = 0
		var g:CGFloat = 0
		var b:CGFloat = 0
		var a:CGFloat = 0
		color.getRed(&r,green:&g,blue:&b,alpha:&a)
		return (
			Utils.uint8(Int(Utils.clamp(r*255,0,255))),
			Utils.uint8(Int(Utils.clamp(g*255,0,255))),
			Utils.uint8(Int(Utils.clamp(b*255,0,255)))
		)
	}

	static func convertRGBToInt(_ rgb:(r:UInt8,g:UInt8,b:UInt8)) -> Int {
		return (Int(rgb.r) << 16) | (Int(rgb.g) << 8) | Int(rgb.b)
	}

	static func convertIntToRGB(_ int:Int) -> (r:UInt8,g:UInt8,b:UInt8) {
		return (Utils.uint8(int >> 16),Utils.uint8(int >> 8),Utils.uint8(int))
	}

	static func convertIntToColor(_ int:Int) -> UIColor {
		return convertRGBToColor(convertIntToRGB(int))
	}

	static func convertColorToInt(_ color:UIColor) -> Int {
		return convertRGBToInt(convertColorToRGB(color))
	}

	static func lerp(_ a:UIColor,_ b:UIColor,_ t:CGFloat) -> UIColor {
		if t <= 0 { return a }
		if t >= 1 { return b }
		let t1 = 1-t
		var ar:CGFloat = 0
		var ag:CGFloat = 0
		var ab:CGFloat = 0
		var aa:CGFloat = 0
		var br:CGFloat = 0
		var bg:CGFloat = 0
		var bb:CGFloat = 0
		var ba:CGFloat = 0
		a.getRed(&ar,green:&ag,blue:&ab,alpha:&aa)
		b.getRed(&br,green:&bg,blue:&bb,alpha:&ba)
		return UIColor(
			red:ar*t1+br*t,
			green:ag*t1+bg*t,
			blue:ab*t1+bb*t,
			alpha:aa*t1+ba*t
		)
	}
}
