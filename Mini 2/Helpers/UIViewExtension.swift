import Foundation
import UIKit

extension UIView {
	func move(x:CGFloat,y:CGFloat) {
		frame = CGRect(
			x:x+frame.minX,
			y:y+frame.minY,
			width:frame.width,
			height:frame.height
		)
	}

	func dragShadowInit() {
		layer.masksToBounds = false
		layer.shadowColor = Color.darkGray.cgColor
		layer.shadowOpacity = 0
		layer.shadowOffset = CGSize(width:0,height:2)
		layer.shadowRadius = 5
	}

	func dragShadow(_ dragging:Bool,resize:Bool = false) {
		let duration:Double = 0.15
		let opacity:Float = (dragging ? 0.4 : 0)

		let oldOpacity = layer.shadowOpacity
		layer.shadowOpacity = opacity
		let animOpacity = CABasicAnimation.init(keyPath:"shadowOpacity")
		animOpacity.duration = duration
		animOpacity.fromValue = oldOpacity
		animOpacity.toValue = opacity
		layer.add(animOpacity,forKey:"shadowOpacity")

		if resize {
			let scale:CGFloat = (dragging ? 1.1 : 1)
			UIView.animate(withDuration: duration,delay:0,options:UIViewAnimationOptions(),animations:{
				self.transform = CGAffineTransform(scaleX: scale,y: scale)
			},completion:nil)
		}
	}
}
