import Foundation
import UIKit

class Utils {
	static func clamp<T:Comparable>(_ t:T,_ a:T,_ b:T) -> T{
		if t <= a { return a }
		if t >= b { return b }
		return t
	}

	static func dir(_ old:Int,_ new:Int) -> Int {
		if new > old { return 1 }
		if new < old { return -1 }
		return 0
	}

	static func pointSample(_ view:UIImageView) {
		view.layer.magnificationFilter = kCAFilterNearest
		view.layer.minificationFilter = kCAFilterLinear
	}

	static func uint8(_ int:Int) -> UInt8 {
		return UInt8(truncatingBitPattern:int)
	}

	static func copyImage(_ image:UIImage) -> UIImage {
		return UIImage(data:UIImagePNGRepresentation(image)!)!
	}

	static func randomNumber(_ range:ClosedRange<Int> = 1...3) -> Int {
		let min = range.lowerBound
		let max = range.upperBound
		return Int(arc4random_uniform(UInt32(max-min)))+min
	}
}
