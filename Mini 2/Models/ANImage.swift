import Foundation
import UIKit

class ANImage {
	let width:Int
	let height:Int
	let length:Int

	fileprivate var buffer:[UInt8]

	fileprivate(set) var min:Int
	fileprivate(set) var max:Int
	fileprivate(set) var trackingChanges:Bool

	fileprivate static let colorSpaceRef = CGColorSpaceCreateDeviceRGB()

	convenience init() {
		self.init(width:Comum.defaultSize,height:Comum.defaultSize)
	}

	init(copy:ANImage) {
		width = copy.width
		height = copy.height
		length = width*height
		buffer = copy.buffer
		min = -1
		max = -1
		trackingChanges = false
	}

	init(width:Int,height:Int) {
		self.width = width
		self.height = height
		length = width*height
		buffer = [UInt8](repeating: 0xff,count: width*height*4)
		min = -1
		max = -1
		trackingChanges = false
	}

	func copy(_ from:ANImage) {
		if width != from.width || height != from.height { return }
		buffer = from.buffer
		min = -1
		max = -1
		trackingChanges = false
	}

	func setSafeCheck(_ x:Int,_ y:Int,color:(r:UInt8,g:UInt8,b:UInt8)) -> Bool {
		if x < 0 || x >= width || y < 0 || y >= height { return false }
		return setCheck(x,y,color:color)
	}

	func setCheck(_ x:Int,_ y:Int,color:(r:UInt8,g:UInt8,b:UInt8)) -> Bool {
		let pos = getPosAt(x,y)
		if isEqual(pos,color:color) { return false }
		set(pos,color:color)
		return true
	}

	func set(_ pos:Int,color:(r:UInt8,g:UInt8,b:UInt8)) {
		let b = pos*4
		buffer[b+0] = color.r
		buffer[b+1] = color.g
		buffer[b+2] = color.b
		if trackingChanges {
			if min < 0 {
				min = pos
				max = pos
			} else if min > pos {
				min = pos
			} else if max < pos {
				max = pos
			}
		}
	}

	func getSafe(_ x:Int,_ y:Int) -> (r:UInt8,g:UInt8,b:UInt8) {
		if x < 0 || x >= width || y < 0 || y >= height { return (0xff,0xff,0xff) }
		return get(x,y)
	}

	func get(_ x:Int,_ y:Int) -> (r:UInt8,g:UInt8,b:UInt8) {
		return get(getPosAt(x,y))
	}

	func get(_ pos:Int) -> (r:UInt8,g:UInt8,b:UInt8) {
		let b = pos*4
		return (buffer[b+0],buffer[b+1],buffer[b+2])
	}

	func isEqualSafe(_ x:Int,_ y:Int,color:(r:UInt8,g:UInt8,b:UInt8)) -> Bool {
		if x < 0 || x >= width || y < 0 || y >= height { return false }
		return isEqual(x,y,color:color)
	}

	func isEqual(_ x:Int,_ y:Int,color:(r:UInt8,g:UInt8,b:UInt8)) -> Bool {
		return isEqual(getPosAt(x,y),color:color)
	}

	func isEqual(_ pos:Int,color:(r:UInt8,g:UInt8,b:UInt8)) -> Bool {
		let b = pos*4
		return buffer[b+0] == color.r && buffer[b+1] == color.g && buffer[b+2] == color.b
	}

	func isEqual(_ pos:Int,image:ANImage) -> Bool {
		let b = pos*4
		return buffer[b+0] == image.buffer[b+0] && buffer[b+1] == image.buffer[b+1] && buffer[b+2] == image.buffer[b+2]
	}

	func getPosAt(_ x:Int,_ y:Int) -> Int {
		return x+y*width
	}

	func startTrackingChanges() {
		trackingChanges = true
		min = -1
		max = -1
	}

	func stopTrackingChanges() {
		trackingChanges = false
	}

	func createImage(_ copy:Bool) -> UIImage {
		let prov = CGDataProvider(dataInfo:nil,data:buffer,size:width*height*4,releaseData:{(a,b,c) in})!
		let cg = CGImage(width: width,height: height,bitsPerComponent: 8,bitsPerPixel: 32,bytesPerRow: 4*width,space: ANImage.colorSpaceRef,bitmapInfo: CGBitmapInfo(rawValue:CGImageAlphaInfo.noneSkipLast.rawValue),provider: prov,decode: nil,shouldInterpolate: false,intent: CGColorRenderingIntent.defaultIntent)!
		let image = UIImage(cgImage:cg)
		if !copy { return image }
		return Utils.copyImage(image)
	}

	func importImage(_ image:UIImage) {
		let pixelData = image.cgImage?.dataProvider?.data
		let data:UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
		let imageWidth = Int(image.size.width)
		let imageHeight = Int(image.size.height)
		if width == imageWidth {
			let m = Swift.min(width*height,imageWidth*imageHeight)
			for i in 0..<m {
				let b = i*4
				buffer[b+0] = data[b+0]
				buffer[b+1] = data[b+1]
				buffer[b+2] = data[b+2]
			}
		} else {
			let w = Swift.min(width,imageWidth)
			let h = Swift.min(height,imageHeight)
			for x in 0..<w {
				for y in 0..<h {
					let b = (x+y*imageWidth)*4
					let p = getPosAt(x,y)*4
					buffer[p+0] = data[b+0]
					buffer[p+1] = data[b+1]
					buffer[p+2] = data[b+2]
				}
			}
		}
	}
}
