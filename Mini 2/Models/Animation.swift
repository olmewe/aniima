import Foundation
import UIKit

class Animation {
	var info:AnimationInfo
	var frames:[UIImage]

	init() {
		info = AnimationInfo()
		frames = [UIImage]()
	}

	var frameDuration:Double {
		if prevFps != info.fps {
			prevFps = info.fps
			prevDuration = 1/Double(info.fps)
		}
		return prevDuration
	}
	fileprivate var prevFps:Int = 0
	fileprivate var prevDuration:Double = 0
}

class AnimationInfo:NSObject,NSCoding {
	var name:String
	var id:String
	var fps:Int
	var pos:Int
	var date:Date
	var new:Bool = true

	override init() {
		name = "untitled"
		id = ""
		fps = Comum.defaultFps
		pos = 0
		date = Date()
	}

	required init?(coder decoder:NSCoder) {
		guard
			let name = decoder.decodeObject(forKey: "name") as? String,
			let id = decoder.decodeObject(forKey: "id") as? String
			else {
			return nil
		}
		self.name = name
		self.id = id
		fps = decoder.decodeInteger(forKey: "fps")
		if let date = decoder.decodeObject(forKey: "date") as? Date {
			self.date = date
		} else {
			date = Date()
		}
		pos = 0
	}

	func encode(with coder:NSCoder) {
		coder.encode(name,forKey:"name")
		coder.encode(id,forKey:"id")
		coder.encode(fps,forKey:"fps")
		coder.encode(date,forKey:"date")
	}
}
