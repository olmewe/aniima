import UIKit

class Parents {

	var parentsOne : Int!
	var parentsTwo : Int!

	init(one : Int!, two : Int!){
		self.parentsOne = one
		self.parentsTwo = two
	}

	func getParentsOne() -> Int!{
		return self.parentsOne
	}

	func getParentsTwo() -> Int! {
		return self.parentsTwo
	}

}
