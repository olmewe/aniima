import Foundation
import UIKit

class FrameView:UIView {
	weak var canvas:CanvasViewController!
	fileprivate var start = true
	fileprivate var currentIndex:Int = 0

	@IBOutlet weak var tapGesture:UITapGestureRecognizer!

	@IBOutlet fileprivate weak var index:UILabel!
	@IBOutlet fileprivate weak var imageView:UIImageView!
	@IBOutlet fileprivate weak var circle:UIImageView!
	var image:UIImage? {
		get {
			if let img = imageView {
				return img.image
			}
			return nil
		}
		set {
			if let img = imageView {
				img.image = newValue
			}
		}
	}

	enum FrameSelected {
		case `false`
		case `true`
		case onion
	}
	var selected:FrameSelected {
		get {
			return _selected
		}
		set {
			_selected = newValue
			switch newValue {
			case .false: backgroundColor = Color.frameBorderUnselected
			case .true: backgroundColor = Color.frameBorderSelected
			case .onion: backgroundColor = Color.frameBorderOnion
			}
			circle.tintColor = backgroundColor
		}
	}
	fileprivate var _selected:FrameSelected = .false

	override func didMoveToSuperview() {
		if !start { return }
		start = true
	}

	@IBAction func tap(_ sender:AnyObject) {
		canvas.setFrame(currentIndex,edit:true)
	}

	func updateIndex(_ i:Int) {
		self.currentIndex = i
		let i = i+1
		if i < 1 {
			index.text = ""
		} else if i < 10 {
			index.text = "\(i)   "
		} else if i < 100 {
			index.text = "\(i) "
		//} else if i < 1000 {
		//	index.text = "\(i) "
		} else {
			index.text = "\(i)"
		}
	}
}
